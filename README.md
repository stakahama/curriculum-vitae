To run: 
```sh
$ ./compile 
```
or
```sh
$ python compile
```
The script `compile` calls `reorderbbl.py` on the `.bbl` file generated after the call to bibtex.

Run `txt2bib.py` to extract list of presentations and posters from `presentations.txt` to `mypresentations.bib` file.

Notes:
*    `takahama-cv.tex` is main file
*    `takahama-cv-2pgs.tex` is short CV format (infrequently updated)

Run `./publish` to copy `takahama-cv.pdf` to `../../public/curriculum-vitae` and push results to public GitHub repository.

