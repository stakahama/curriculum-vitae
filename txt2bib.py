#!/usr/bin/python

import re
import operator

def uni2qte(x):
    return reduce(lambda s,x: s.replace(*x),
                  [('\xe2\x80\x9d','"'),
                   ('\xe2\x80\x9c','"'),
                   ('\xe2\x80\x99',"'"),
                   ('&','\&')],
                  x)

def readfile(x,eoc=re.compile('\n\n?[ ]?\n')):
    with open(x,'r') as f:
        chunks = eoc.split(f.read())
        out_1 = []
        for elem in chunks:
            if len(elem.strip('\n'))==0:
                continue
            out_2 = []
            for line in elem.split('\n'):
                line = uni2qte(line)
                out_2.append(line)
            out_1.append(out_2)
    return out_1

def arrange_authors(x,
                    sep=re.compile(',[ ]?'),
                    nre=re.compile('(.+)[ ]([^ -*]+)(\\*?)$')):
    names = sep.split(x.strip().strip('.'))
    lastfirst = []
    for nm in names:
        out = nre.search(nm).groups()
        lastfirst.append('%s, %s%s' % (out[1],out[0],out[2])) # include star
    return ' and '.join(lastfirst)

def arrange_dates(x,
                  patt=re.compile('(.+)[.,] ([A-Za-z]{3})[.,]{0,2}[ ]?([0-9]{4})[ .]?$')): # exclude period for abbreviation of month
    sp = list(patt.search(x).groups())
    return [sp[0],' '.join(sp[1:])]

def get_fields(x,
               form=re.compile('([0-9]+)?\\.?[ \t]*\\((.+)\\)[ ]\\"([^"]+)\\"[ ](.+)'),
               fieldnames=['conference','year','number','notes','title','author']):
    conference = arrange_dates(x[0]) # x[:1]
    rest = x[1:]
    fields = []
    for e in rest:
        if e=='':
            continue
        fds = list(form.search(e).groups())
        fds[-1] = arrange_authors(fds[-1])
        fds[-2] = fds[-2].strip('.') # title
        fields.append(dict(zip(fieldnames,conference+fds)))
    return fields

def format_bibtex_entry(x,id):
    d = x.copy()
    del d['number']
    fields = map(lambda x,y: '  %s = {%s}' % (x,y),
                 d.keys(),d.values())
    return '@misc{%s,\n%s\n}' % (id,',\n'.join(fields))

def my_presentations(x):
    yearlist = set(map(operator.itemgetter('year'),x))
    yearids = dict(zip(yearlist,[0]*len(yearlist)))
    keep = []
    for e in x:
        elem = e.copy()
        if 'Takahama, S.*' not in elem['author'] or \
               elem['notes'].lower() not in ['presentation','invited talk']:
            continue
        elem['author'] = elem['author'].replace('*','')
        elem['title'] = '{"%s," %s}' % (elem['title'],elem['conference'])
        del elem['conference']
        yearids[elem['year']] += 1
        keep.append(format_bibtex_entry(elem,'%s_%d' % (elem['year'].lower().replace(' ','_'),yearids[elem['year']])))
    return keep

def export_bibfile(x,filename):
    with open(filename,'w') as f:
        f.write('\n\n'.join(x))

out = readfile('presentations.txt')
flattened = reduce(lambda x,y: x + get_fields(y),out,[])
mine = my_presentations(flattened)
export_bibfile(mine,'mypresentations.bib')

# #debugging:
# out = readfile('presentations.txt')
# y = []
# for x in out:
#     y.append(get_fields(x))
