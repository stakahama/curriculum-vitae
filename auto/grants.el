(TeX-add-style-hook
 "grants"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "10pt" "a4paper")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("geometry" "hmargin=2.5cm" "vmargin=2.5cm")))
   (TeX-run-style-hooks
    "latex2e"
    "article"
    "art10"
    "geometry"
    "parskip"
    "hanging")
   (LaTeX-add-environments
    "mylist")
   (LaTeX-add-lengths
    "indentation"
    "indentationsecond"))
 :latex)

