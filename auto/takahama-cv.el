(TeX-add-style-hook
 "takahama-cv"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "10pt" "a4paper")))
   (TeX-run-style-hooks
    "latex2e"
    "preamble"
    "article"
    "art10"))
 :latex)

