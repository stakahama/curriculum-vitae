#!/usr/bin/python

import re
import os
import sys
import time
import operator

# #for elsart-harv.bst or copernicus.bst
# PATT = re.compile('.+bibitem\\[\\{([A-Z-.]+)[ ].+([0-9]{4}).+',
#                   re.IGNORECASE)
BIBKEY = re.compile('.bibitem\\[([^]]+)\\].+')
PATT = re.compile('([^(]+)\\(([A-Za-z]{0,3})\\.?[ ]?([0-9]{4})[^)]*\\)(.*)')
FIELDS = ['AuthAbbrev','Month','Year','AuthList']
SEP = '\n\n'

def get_year_authors(x):
    # called by sortby_year_authors
    key = BIBKEY.search(x).group(1)[1:-1].replace('\n','')
    result = PATT.search(key)
    if result:
        res_ = dict(zip(FIELDS,result.groups()))
        res_['Month'] = time.strptime('Feb','%b')[1] if len(res_['Month']) > 0 \
                        else 999
        res_['AuthList'] = res_['AuthList'] if len(res_['AuthList']) > 0 \
                           else res_['AuthAbbrev']
        return (int(res_['Year']), res_['Month'], res_['AuthList'], x)
    else:
        return (999, 999, 999, x)

def sortby_year_authors(bibitems):
    # calls get_year_authors
    # called by reorder
    triples = map(get_year_authors,bibitems)
    by_year = {}
    for t in triples:
        key = '%d_%d' % t[:2]
        if key not in by_year.keys():
            by_year[key] = []
        by_year[key].append(t[2:])
    sortedkeys = sorted(by_year.keys())
    sortedkeys.reverse() # descending
    orderedlist = []
    for s in sortedkeys:
        by_year[s].sort() # sort by author (ascending) within year
        orderedlist += map(operator.itemgetter(-1),by_year[s])
    return orderedlist
    # refs = map(lambda x: x[-1],sorted(triples))
    # refs.reverse()
    # return refs

def reorder(inpfile):
    # calls sortby_year_authors
    outfile = inpfile+'~'
    with open(outfile,'w') as fout:
        with open(inpfile,'r') as finp:
            txt = finp.read()
        first = txt.find('\\bibitem')
        last = txt.find(SEP+'\\end{thebibliography}')
        entries = txt[first:last].split(SEP)
        fout.write(txt[:first]+SEP)
        fout.write(SEP.join(sortby_year_authors(entries)))
        fout.write(txt[last:])
    os.rename(outfile,inpfile)
    print 'reordered ' + inpfile

filename = sys.argv[-1]
reorder(filename+'.bbl')
